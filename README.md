# policy-ci
---
Gitlab CI template for policy cli.

## Templates
---
### .policy-base

This template defines the shared configuration for all the policy CI Templates.
The template is not supposed to run in any job.

#### Variables

##### POLICY_IMAGE_VERSION

**Required.** The version of the policy docker image to use.
**Default**: `0.1`

##### POLICY_LOG_VERBOSITY: 

**Optional.** The verbosity of the the policy CLI. Can be `info`, `error`, `debug` or `trace`.
**Default**: error

##### POLICY_FILE_STORE_ROOT: './.policy-store'

**Required.** The root directory for the policy store.
**Default**: './.policy-store'.

### policy-build

Gilab CI template for building policy images. It uses the variables defined in `lib/gitlab/templates/Policy/Base.gitlab-ci.yml`. This template can be chained with other Policy CI templates as it outputs the policy store directory defined in `POLICY_FILE_STORE_ROOT` as a path artifact.

#### Variables

##### POLICY_TAG

**Required.** The build tag applied to the created policy image.

##### POLICY_SRC

**Required.** The directory path of the input source to build.
**Default**: 'src' 

##### POLICY_REVISION

**Required.** The commit SHA of the input source to build. 
**Default**: `$CI_COMMIT_SHA`

### policy-login

Gilab CI template for logging into a policy image repository. It uses the variables defined in `lib/gitlab/templates/Policy/Base.gitlab-ci.yml`.

This template can be chained with other Policy CI templates as it
outputs the policy store directory defined in `POLICY_FILE_STORE_ROOT` as a path artifact.

#### Variables

##### POLICY_USERNAME

**Required.** The username of the policy image repository.

##### POLICY_PASSWORD

**Required.** The password of the policy image repository.

##### POLICY_DEFAULT_DOMAIN

**Required.** The domain of the policy image repository.

### policy-logout

Gilab CI template to log-out of a policy image repository. It uses the variables defined in `lib/gitlab/templates/Policy/Base.gitlab-ci.yml`.

This template can be chained with other Policy CI templates as it outputs the policy store directory defined in `POLICY_FILE_STORE_ROOT` as a path artifact.

### policy-push

Gilab CI template to push a policy image to a policy image repository. It uses the variables defined in `lib/gitlab/templates/Policy/Base.gitlab-ci.yml`.

This template can be chained with other Policy CI templates as it outputs the policy store directory defined in `POLICY_FILE_STORE_ROOT` as a path artifact.

#### Variables

##### POLICY_TARGET_TAGS

**Required.** The tags to push to the policy image repository. It accepts a comma separated list of tags.

##### POLICY_DEFAULT_DOMAIN
**Required.** The domain of the policy image repository.

### policy-tag

Gilab CI template that create a tag for a policy image. It uses the variables defined in `lib/gitlab/templates/Policy/Base.gitlab-ci.yml`.

This template can be chained with other Policy CI templates as it outputs the policy store directory defined in `POLICY_FILE_STORE_ROOT` as a path artifact.

#### Variables

##### POLICY_TAG
**Required.** The source tag of the policy image.

##### POLICY_TARGET_TAGS
**Required.** The tags to push to the policy image repository. It accepts a comma separated list of tags.

##### POLICY_DEFAULT_DOMAIN
**Required.** The domain of the policy image repository.

## Example
---
[This](https://gitlab.com/aserto/policy-ci-sample) repo shows an example on how to use the [policy-ci](https://gitlab.com/aserto/policy-ci) together with the [sver-ci](https://gitlab.com/aserto/sver-ci) CI templates to implement CI/CD for a policy image.
